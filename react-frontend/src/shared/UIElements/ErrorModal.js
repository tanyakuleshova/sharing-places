import React from 'react';

import Modal from '../Modal/Modal';

const ErrorModal = props => {
  return (
    <Modal
      onCancel={props.onClear}
      header="An Error Occurred!"
      show={!!props.error}
      footer={<div onClick={props.onClear}>Okay</div>}
    >
      <p>{props.error}</p>
    </Modal>
  );
};

export default ErrorModal;
