import React from "react";

const Avatar = props => {
    return (
        <img src={props.image} className="rounded-md max-h-24"/>
        // <img src={props.image} className={`avatar ${props.className}`} style={props.style}/>
    );
};

export default Avatar;