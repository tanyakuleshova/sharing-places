import React, { useRef, useEffect } from 'react';
import {Helmet} from "react-helmet";

const GoogleMap = props => {

    console.log(props.coordinates);
    const initGoogleMap = () => {
        let map = new google.maps.Map(document.getElementById("map"), {
            center: {lat: props.coordinates.lat, lng: props.coordinates.lng},
            zoom: props.zoom,
        });

        let marker = new google.maps.Marker({
            position: {lat: props.coordinates.lat, lng: props.coordinates.lng},
            map: map,
        });

    }

    if(!window.initMap){
        window.initMap = initGoogleMap;
    }else{
        // window.initMap = null;
        const el = document.querySelector(".map-script");
        console.log(el);

        if(el){
            el.remove();
            window.initMap = initGoogleMap;
        }

    }
    return (
        <div>
            <div id="map" className="h-96 w-full"></div>
           <Helmet>
                <script className="map-script" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwdj321xEIC4qKG_6cDt8g_UlnRk9YxJI&callback=initMap"
                        type="text/javascript" />
            </Helmet>

        </div>
    );
};

export default GoogleMap;