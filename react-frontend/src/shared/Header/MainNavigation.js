import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import MainHeader from "./MainHeader";
import NavLinks from "./NavLinks";

const MainNavigation = props => {
    const [isMenuShown, setMenuShown] = useState(true);
    const text = isMenuShown ? 'hide menu' : 'show menu';
    return (
        <React.Fragment>
            {isMenuShown && (<MainHeader>
                <nav>
                    <NavLinks/>
                </nav>
            </MainHeader>)}
            <div onClick={()=>{setMenuShown(!isMenuShown)}} className="cursor-pointer text-gray-400 m-4 hover:text-yellow-500">{text}</div>
        </React.Fragment>
    );
};

export default MainNavigation;
