import React from "react";
import ReactDOM from "react-dom";
import {CSSTransition} from "react-transition-group";

const MainHeader = props => {
    const content =
            <header className="p-4 bg-gray-100">
                {props.children}
            </header>;

    return ReactDOM.createPortal(content, document.getElementById('header'));
};

export default MainHeader;