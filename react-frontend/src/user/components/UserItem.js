import React from "react";
import Avatar from "../../shared/UIElements/Avatar";
import {Link} from "react-router-dom";
const UserItem = props => {
    return (
        <li className="w-1/3 p-3">
         <Link to={`/${props.id}/places`}>
             <div className="w-full rounded-md flex bg-black hover:text-white hover:bg-yellow-500 text-yellow-500 shadow-md p-4">
                 <Avatar image={`http://localhost:5001/${props.image}`}/>
                 <div>
                     <div className=" text-2xl ml-4">{props.name}</div>
                     <div className="text-white text-lg ml-4">Shared places: {props.placeCount}</div>
                 </div>
             </div>
         </Link>
        </li>
    );
};

export default UserItem;