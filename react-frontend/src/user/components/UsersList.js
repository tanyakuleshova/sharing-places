import React from "react";
import UserItem from "./UserItem";

const UsersList = props => {
    if(props.items.length === 0){
        return <div className="text-center">No items here.</div>;
    }

    return (
        <div className="m-4">
            <div>
                <h1 className="text-xl my-2">Users List</h1>
                <ul className="flex flex-wrap w-full">

                       {props.items.map(user =>{
                           return <UserItem key={user.id} id={user.id} image={user.image} name={user.name} placeCount={user.places.length}/>;
                       })}

                </ul>
            </div>
        </div>
    );
};

export default UsersList;