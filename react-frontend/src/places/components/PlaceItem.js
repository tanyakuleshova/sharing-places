import React, {useState, useContext} from "react";
import {Link} from "react-router-dom";
import Modal from "../../shared/Modal/Modal";
// import Map from "../../shared/Map/Map";
import GoogleMap from "../../shared/Map/GoogleMap";
import {AuthContext} from "../../shared/context/auth-context";
import {useHttpClient} from "../../shared/hooks/http-hook";
import LoadingSpinner from "../../shared/UIElements/LoadingSpinner";
import ErrorModal from "../../shared/UIElements/ErrorModal";
const PlaceItem = props => {
    const { isLoading, error, sendRequest, clearError } = useHttpClient();
    const auth = useContext(AuthContext);
    const [showMap, setShowMap] = useState(false);
    const [showConfirmModal, setShowConfirmModal] = useState(false);
    const openMapHandler = ()=>setShowMap(true);
    const closeMapHandler = ()=>setShowMap(false);
    const showDeleteWarningHandler = () => {
        setShowConfirmModal(true);
    };

    const cancelDeleteHandler = () => {
        setShowConfirmModal(false);
    };

    const confirmDeleteHandler = async () => {
        setShowConfirmModal(false);
        try {
            await sendRequest(
                `http://localhost:5001/api/places/${props.id}`,
                'DELETE'
            );
            props.onDelete(props.id);
        } catch (err) {}
    };
    return (
        <React.Fragment>
            <ErrorModal error={error} onClear={clearError} />
            <Modal
                show={showMap}
                onCancel={closeMapHandler}
                header={props.address}
                contentClass="p-0"
                footerClass="text-center"
                footer={<div className="cursor-pointer" onClick={closeMapHandler}>Close</div>}
            >
                <div className="h-96 w-full">
                    {/*<Map center={props.coordinates} zoom={16} />*/}
                    <GoogleMap coordinates={props.coordinates} zoom={10} />
                </div>
            </Modal>
            <Modal
                show={showConfirmModal}
                onCancel={cancelDeleteHandler}
                header="Are you sure?"
                footerClass="place-item__modal-actions"
                footer={
                    <React.Fragment>
                        <button className="mr-3" onClick={cancelDeleteHandler}>
                            CANCEL
                        </button>
                        <button onClick={confirmDeleteHandler}>
                            DELETE
                        </button>
                    </React.Fragment>
                }
            >
                <p>
                    Do you want to proceed and delete this place? Please note that it
                    can't be undone thereafter.
                </p>
            </Modal>
            <li className="">
                <div className="bg-gray-800 text-yellow-500 hover:text-white hover:bg-yellow-500 cursor-pointer rounded-md pb-3">
                    {isLoading && <LoadingSpinner asOverlay />}
                    <div className="h-96 bg-no-repeat bg-center bg-cover rounded-md" style={{
                        backgroundImage: `url("http://localhost:5001/${props.image}")`
                    }}></div>
                    <div className="text-lg my-2 text-center font-bold">{props.title}</div>
                </div>
                <div className="mt-4">
                    {auth.userId === props.creattorID && <Link to={`/places/${props.id}`} >Edit</Link>}
                    <div onClick={openMapHandler}>Open Map</div>
                    {auth.userId === props.creattorID && <div onClick={showDeleteWarningHandler}>DELETE</div>}
                </div>
            </li>
            </React.Fragment>)
};
export default PlaceItem;