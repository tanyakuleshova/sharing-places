import React from "react";
import PlaceItem from "./PlaceItem";
import {Link} from "react-router-dom";

const PlaceList = props => {
    if(props.items.length === 0){
        return <div className="flex">
            <div className="w-1/3 mx-auto p-4 bg-gray-100 rounded-md">
                <div>
                    No Places
                </div>
               <div className="flex">
                   <Link to="/places/new" className="mt-4 mx-auto text-center p-2 bg-yellow-500 rounded-md text-white cursor-pointer">Share place</Link>
               </div>
            </div>

        </div>;
    }
    return (
        <div className="flex justify-center mx-auto w-1/2">
            <ul className="w-1/2">
                {props.items.map(place =>
                    <PlaceItem key={place.id} id={place.id}
                               image={place.image} title={place.title}
                               description={place.description} address={place.address}
                               coordinates={place.location} creattorID={place.creator}
                               onDelete={props.onDeletePlace}
                    />)}
            </ul>
        </div>);
};
export default PlaceList;